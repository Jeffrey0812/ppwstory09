
function onSignIn(googleUser) {
    var id_token = googleUser.getAuthResponse().id_token;
    var profile = googleUser.getBasicProfile();    
    sendToken(id_token);
    console.log("Logged In")
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}
function signOut() {
var auth2 = gapi.auth2.getAuthInstance();
auth2.signOut().then(function () {
  console.log('User signed out.');
});
}

var sendToken = function(token) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/login",
        headers:{
            "X-CSRFToken": csrftoken
        },
        data: {id_token: token },
        success: function(result) {
            console.log("Signed in");
            if (result.status === "0") {
                window.location.replace(result.url)
            } else {
                html = "<h1>Something wrong, please contact website admin</h3>";
                $("h1").replaceWith(html)
            }
        },
        error: function (error) {
            alert("Something wrong, please contact website admin")
        }
    })
};
