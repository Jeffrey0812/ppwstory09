from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, search_book, addFavorite
from .models import Book

class Story09Test(TestCase):
    def test_story09_url_is_exist(self):
        response = Client().get('/library/')
        self.assertEqual(response.status_code,200)

    def test_story09_using_index_func(self):
        found = resolve('/library/')
        self.assertEqual(found.func, index)

    def test_title_library(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn("Story 09 - Books", html_response)

    def test_model_create_new_book(self):
        Book.objects.create(title='Flying')
        count = Book.objects.count()
        self.assertEqual(count,1)

