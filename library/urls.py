from django.urls import path
from django.conf.urls import url, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('search_book/', views.search_book, name='search_book'),
    path('search_new_book/', views.search_new_book, name='search_new_book'),
    path('addFavorite/', views.addFavorite, name='addFavorite'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^auth/', include('social_django.urls', namespace='social')),  # <- Here 
]