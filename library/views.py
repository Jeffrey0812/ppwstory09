from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect
import requests
from django.views.decorators.csrf import csrf_exempt
from .models import Book
from google.oauth2 import id_token
from google.auth.transport import requests
from django.urls import reverse

# Create your views here.
def index(request):
    count = Book.objects.count()
    return render(request, 'library/index.html', {'count': count})

@csrf_exempt
def search_book(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=quilting"
    req = requests.get(url)
    resp = req.json()
    all_data = []
    for x in resp['items']:
        title = x['volumeInfo']['title']
        authors = x['volumeInfo']['authors']
        publishedDate = x['volumeInfo']['publishedDate']
        dic = {"title": title, "author":authors, "publishedDate":publishedDate}
        all_data.append(dic)
    return JsonResponse({'data': all_data})

@csrf_exempt
def search_new_book(request):
    if request.method == 'POST':
        query = request.POST['query']
        url = "https://www.googleapis.com/books/v1/volumes?q=" + query
        req = requests.post(url)
        resp = req.json()
        all_data = []
        for x in resp['items']:
            title = x['volumeInfo']['title']
            authors = x['volumeInfo']['authors']
            publishedDate = x['volumeInfo']['publishedDate']
            dic = {"title": title, "author":authors, "publishedDate":publishedDate}
            all_data.append(dic)
        return JsonResponse({'data': all_data})

@csrf_exempt
def addFavorite(request):
    if request.method == 'POST':
        title = request.POST['title']
        data = Book(title=title)
        data.save()
        return JsonResponse({'title': data.title})

def login(request):
    if request.method == "POST":
        try:
            token = request.POST['id_token']
            id_info = id_token.verify_oauth2_token(token, requests.Request(), "632246741161-gh0qkdvu8l084l5v7im4i9r5rsifb5k1.apps.googleusercontent.com")
            if id_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')
            userid = id_info['sub']
            email = id_info['email']
            name = id_info['name']
            request.session['user_id'] = userid
            request.session['email'] = email
            request.session['name'] = name
            request.session['book'] = []
            return JsonResponse({"status": "0", 'url': reverse("")})
        except ValueError:
            return JsonResponse({"status": "1"})
    return render(request, 'library/login.html')


def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login'))
